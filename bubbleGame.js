
let  score = 0;
var tmeo;

function drawCircle(imgID=0) {

    var div = document.getElementById("div");
    var img = document.createElement("img");
    img.src = "crc1.png";
    img.className = "img";
    img.id = imgID;
  
    img.addEventListener('click', function() {
         deleteImg(img.id);
         score+=10;
         checkScore();
    })

    img.style.bottom = "0px";
    img.style.position = "absolute";
    div.appendChild(img);

    var pos = div.offsetWidth-img.clientWidth;
    var randomPos = Math.floor(Math.random()*pos);
    img.style.left = randomPos + 10 + 'px';
    
    tmeo =  setTimeout(function() {
            drawCircle(++imgID);
          }, countSec);
          moveUp(imgID);
 

    if(imgID >= 50 || seconds == 0) {
        clearTimeout(tmeo);
        return;
        
    }   
}

let seconds;

function startGame() {
    seconds = 20;
    drawCircle();
    calculateTime();
    let button = document.getElementById("butt");
    button.style.display = 'none';
    let scoreInfo = document.getElementById('scoreDiv');
    score = 0;
    scoreInfo.innerHTML = "Score - " + score;

    // var replay = document.createElement("button");
	// replay.id = "replay";
	// document.body.appendChild(replay);
	// replay.innerHTML = "REPLAY";
	// replay.onclick = clearAll;

}

function moveUp(imgID) {
 
    var move = document.getElementById(imgID);
    if(move) {
    move.style.bottom = parseInt(move.style.bottom) + 1 + 'px';
    if(imgID >= 50) {
        if( parseInt( move.style.bottom ) >= 600 ){
        checkGame();
        }
    }        


    if( parseInt( move.style.bottom ) < 600 ) {
       let tme0 = setTimeout( function() {
     moveUp( imgID ); } , speed);
      
    } else {
        deleteImg(imgID);
        return;
        }
    }
}

function deleteImg(imgID) {
    let img = document.getElementById(imgID);
    img.parentNode.removeChild(img);

}

function checkGame() {
    let timer = document.getElementById("timer");
    if(score < 150) {
        timer.innerHTML = "GAME OVER";
        
    } else {
        timer.innerHTML = "YOU WON!";
    }

    let button = document.getElementById("butt");
    button.style.display = 'block';
    button.style.height = '180px';
    button.style.width = '180px';
    button.innerHTML = "PLAY AGAIN";

}

function checkScore() {
    let scoreInfo = document.getElementById('scoreDiv');
    scoreInfo.innerHTML = 'Score - ' + score;
    
}

function calculateTime() {

    let timerInfo = document.getElementById('timer');
    let tminf = setInterval(() => {
    timerInfo.innerHTML = seconds--;
      if(seconds == -2) {
          checkGame();
        clearInterval(tminf);
        storeResult();
    }
    }, 1000);
    
}

let speed;
let countSec;
function changeSpeed(sp,sec) {
    speed = sp;
    countSec = sec;
}

function storeResult() {
    window.localStorage.setItem(user, score);
}

function showScores(key) {
    var div = document.getElementById("usersDiv");
    // var ul = document.createElement("ul");
    // ul.id = "ul1";
    // div.appendChild(ul);    
    // for(var i=0, len=localStorage.length; i<len; i++) {
    //     var li = document.createElement("li");
    //     ul.appendChild(li);
    //     var key = localStorage.key(i);
    //     var value = localStorage[key];
    //     li.innerHTML = key + ' : ' + value;
    // }
    var table = document.createElement("table");
    table.id = "tbl";
    for (var i=0, len=localStorage.length; i<len; i++){
		var row = document.createElement("tr");
		for(var j=0;j<2;j++){
			var cell = document.createElement("td");
			row.appendChild(cell);
            var key = localStorage.key(i);
            var value = localStorage[key];
            cell.innerHTML = key + ' : ' + value;
		}
		table.appendChild(row);
	}
}

var user;

function login() {
    user = document.getElementById("q0").value;
    var span = document.getElementById("span");
    span.innerHTML = "Welcome "+ user;
}